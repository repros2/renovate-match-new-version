# matchNewValue / matchNewVersion

Proposal to add information about the version a dependency is upgraded to in the MR.

Following `matchCurrentValue` and `matchCurrentVersion` this could be `matchNewValue` and `matchNewVersion`, resp.

In this hypothetical repo, two packages are installed via NPM: 

* `foo` with the hypothetical latest version 1.0.0
* `bar` with the hypothetical latest version 1.2.0

Renovate would create an MR labelled `Initial major update` for foo, and an MR labelled `Initial minor update` for bar.

That is all.
